name             'gitlab_omnibus'
maintainer       'Drew Blessing'
maintainer_email 'drew.blessing@mac.com'
license          'Apache 2.0'
description      'Installs/Configures GitLab and GitLab CI Omnibus'
long_description 'Installs and configures GitLab and GitLab CI using GitLab Omnibus packages'
source_url       'https://gitlab.com/dblessing/chef-gitlab_omnibus' if respond_to?(:source_url)
issues_url       'https://gitlab.com/dblessing/chef-gitlab_omnibus/issues' if respond_to?(:issues_url)

version          '1.1.0'

depends 'poise', '~> 2.0'
depends 'packagecloud', '< 1.0'

supports 'centos', '>= 6.5'
supports 'debian', '>= 7.1'
supports 'ubuntu', '>= 12.04'
