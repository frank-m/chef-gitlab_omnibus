Install and configure GitLab and GitLab CI using GitLab Omnibus packages. GitLab
Omnibus packages contain all dependencies needed to run GitLab including Ruby,
PostgreSQL database, etc.

[Issue Tracker](https://gitlab.com/dblessing/chef-gitlab_omnibus/issues)

### Note for Debian 7 users

The `packagecloud` cookbook requires the `apt-transport-https` package. During
integration testing Debian 7.8 required an `apt-get update` in order to download
and install this package. If you get an error about installing this package, try
running `apt-get update` and then run the recipe again. Unfortunately, there wasn't
a clean way to automatically resolve this issue.

## Getting Started

By default, this cookbook does not require any attributes to be set. Simply
include this recipe in a wrapper cookbook or on a node and GitLab will be
installed and configured with `external_url` set to `https://#{node['fqdn']}`.
Override `node['gitlab_omnibus']['external_url']` if the default doesn't fit
your needs.

GitLab CI is not enabled/configured by default. Set
`node['gitlab_omnibus']['ci_external_url']` to enable GitLab CI.

All other configuration values default to the values specified in the GitLab
Omnibus package. See
[gitlab.rb.template](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)
in the GitLab Omnibus repository for default values. For each configuration
key in the template file there is a configuration hash in this cookbook. For
example, to set `gitlab_rails['gitlab_ssh_host']` use
`node['gitlab_omnibus']['gitlab_rails']['gitlab_ssh_host'] = ''`. Similarly,
to set `unicorn['port']` use `node['gitlab_omnibus']['unicorn']['port'] = 8181`.

### Converting YAML configuration to Ruby hash

Values represented in YAML format in `gitlab.rb.template` should be transformed
into nested hashes before being passed in to attributes in this cookbook. One
example of this is the `gitlab_rails['ldap_servers']` configuration key (see
example below).

```ruby
{
  'main' =>
  { 
    'label' => 'LDAP',
    'host' => '_your_ldap_server',
    'port'=>389,
    'uid' => 'sAMAccountName',
    'method' => 'plain',
    'bind_dn' => '_the_full_dn_of_the_user_you_will_bind_with',
    'password' => '_the_password_of_the_bind_user',
    'active_directory' => true,
    'allow_username_or_email_login' => false,
    'block_auto_created_users' => false,
    'base' => '',
    'user_filter' => '',
    'group_base' => '',
    'admin_group' => '',
    'sync_ssh_keys' => false},
  'secondary' =>
  {
    'label' => 'LDAP',
    'host' => '_your_ldap_server',
    'port'=>389,
    'uid' => 'sAMAccountName',
    'method' => 'plain',
    'bind_dn' => '_the_full_dn_of_the_user_you_will_bind_with',
    'password' => '_the_password_of_the_bind_user',
    'active_directory' => true,
    'allow_username_or_email_login' => false,
    'block_auto_created_users' => false,
    'base' => '',
    'user_filter' => '',
    'group_base' => '',
    'admin_group' => '',
    'sync_ssh_keys' => false
  }
}
```

Luckily there is an extremely easy way to convert an example from `gitlab.rb` into a Ruby hash. First, 
open `irb` in a terminal window and require `yaml` library. Copy a YAML example (including `YAML.load` from `gitlab.rb` and paste
into the `irb` window. Press enter and you should get a Ruby hash returned.

```
$ irb -r 'yaml'
irb(main):001:0> YAML.load <<-'EOS' # remember to close this block with 'EOS' below
irb(main):002:0' main: # 'main' is the GitLab 'provider ID' of this LDAP server
irb(main):003:0'   label: 'LDAP'
irb(main):004:0'   host: '_your_ldap_server'
irb(main):005:0'   port: 389
irb(main):006:0'   uid: 'sAMAccountName'
irb(main):007:0'   method: 'plain' # "tls" or "ssl" or "plain"
irb(main):008:0'   bind_dn: '_the_full_dn_of_the_user_you_will_bind_with'
irb(main):009:0'   password: '_the_password_of_the_bind_user'
irb(main):010:0'   active_directory: true
irb(main):011:0'   allow_username_or_email_login: false
irb(main):012:0'   block_auto_created_users: false
irb(main):013:0'   base: ''
irb(main):014:0'   user_filter: ''
irb(main):015:0'   ## EE only
irb(main):016:0'   group_base: ''
irb(main):017:0'   admin_group: ''
irb(main):018:0'   sync_ssh_keys: false
irb(main):019:0' EOS
=> {"main"=>{"label"=>"LDAP", "host"=>"_your_ldap_server", "port"=>389, "uid"=>"sAMAccountName", "method"=>"plain", "bind_dn"=>"_the_full_dn_of_the_user_you_will_bind_with", "password"=>"_the_password_of_the_bind_user", "active_directory"=>true, "allow_username_or_email_login"=>false, "block_auto_created_users"=>false, "base"=>"", "user_filter"=>"", "group_base"=>"", "admin_group"=>"", "sync_ssh_keys"=>false}}
irb(main):020:0>
```

## Backups

By default this cookbook will configure a cron job to backup GitLab daily at
3:00 am and GitLab CI at 3:30 am (if CI is configured). See attributes to configure
custom backup options.

## Enterprise GitLab

This cookbook supports installation of GitLab EE in addition to CE. Enterprise
packages are now available via PackageCloud, too. Set
`node['gitlab_omnibus']['edition'] = 'enterprise'` and GitLab EE will be installed.
You will need to enter a license key in the admin section to continue using GitLab.

## SSL

Omnibus has some magic in it. If you set an `external_url` to some value with
'https' in it Omnibus will enable SSL in Nginx configuration. By default, this
points at `/etc/gitlab/ssl/#{node['fqdn']}.crt` for the certificate and
`/etc/gitlab/ssl/#{node['fqdn']}.key` for the private key. Users of this
cookbook should either place SSL certificates in this location or specify
a new location for certificates with
`node['gitlab_omnibus']['nginx']['ssl_certificate']` and
`node['gitlab_omnibus']['nginx']['ssl_certificate_key']`. This cookbook does
not facilitate handling of SSL certificate files. See
["Things this cookbook doesn't do"](#things-this-cookbook-doesn-t-do) below for
more information.

If GitLab CI is enabled, SSL configuration options for the CI virtual host
should be set. Set `node['gitlab_omnibus']['ci_nginx']['ssl_certificate']` and
`node['gitlab_omnibus']['ci_nginx']['ssl_certificate_key']`. The default
is the same as for GitLab - `/etc/gitlab/ssl/#{node['fqdn']}.crt` for the
certificate and `/etc/gitlab/ssl/#{node['fqdn']}.key` for the private key.

## SSH

GitLab requires OpenSSH. I suggest the
[openssh cookbook](https://supermarket.chef.io/cookbooks/openssh) for managing
SSH. Installing and configuring SSH is outside the scope of this cookbook. See
["Things this cookbook doesn't do"](#things-this-cookbook-doesn-t-do) below for
more information.

## Postfix

GitLab requires Postfix to send email. I recommend the
[postfix cookbook](https://supermarket.chef.io/cookbooks/postfix) for managing
Postfix. Installing and configuring Postfix is outside the scope of this
cookbook. See
["Things this cookbook doesn't do"](#things-this-cookbook-doesn-t-do) below for
more information.
